package com.wwt.orbit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Converts {@code Node}s to JSON.
 * <p>
 * The current implementation uses Jackson 2.x.  We should decide whether we want to make this part of the
 * specification, add in an extra interface or simply leave it as a hidden subject-to-change feature.
 * </p>
 * <p>
 * The current implementation relies on Nodes being MapNodes, which are Maps.  Jackson already "knows" how
 * to serialize maps and thus we need to do no extra work.  This is lazy but highly effective.
 * </p>
 */
public class JacksonJsonifier implements Jsonifier {

    private final ObjectMapper objectMapper;

    @Inject
    JacksonJsonifier(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String toJson(Node root) {
        return objectToJson(root);
    }

    @Override
    public String toJson(Iterable<Node> nodes) {
        return objectToJson(nodes);
    }

    private String objectToJson(Object root) {
        try {
            return objectMapper.writeValueAsString(root);
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }


}
