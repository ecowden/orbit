package com.wwt.orbit;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;

public abstract class Mapper {

    public abstract Node getRootNode();

    abstract MapperContext getMapperContext();

    /**
     * Set the field with the specified name to the specified value.  Name and value cannot be null.
     */
    protected Mapper set(String name, Object value) {
        getRootNode().put(name, value);
        return this;
    }

    /**
     * Set field using the value's class for the name.  The class name is transformed from PascalCase to camelCase.
     * For instance:
     * <pre>
     *     set("hello world");
     * </pre>
     * is equivalent to:
     * <pre>
     *     set("string", "hello world");
     * </pre>
     */
    public Mapper set(Object value) {
        Preconditions.checkNotNull(value, "value cannot be null");
        String camelCaseName = getNameFromClass(value);
        return set(camelCaseName, value);
    }

    public <T> Mapper set(String name, T obj, MappingFunction<T> mappingFunction) {
        mappingFunction.setMapperContext(getMapperContext());
        Node childNode = createNode();
        mappingFunction.evaluate(obj, childNode);
        return set(name, childNode);
    }

    public <T> Mapper list(String name, Iterable<? extends T> objs, MappingFunction<? super T> mappingFunction) {
        List<Node> list = new ArrayList<Node>();
        mappingFunction.setMapperContext(getMapperContext());
        for (T obj : objs) {
            Node childNode = createNode();
            mappingFunction.evaluate(obj, childNode);
            list.add(childNode);
        }
        return set(name, list);
    }

    Node createNode() {
        return getMapperContext().getNodeProvider().get();
    }

    public <T> Mapper set(T value, MappingFunction<T> mappingFunction) {
        String name = getNameFromClass(value);
        return set(name, value, mappingFunction);
    }

    public static String getNameFromClass(Object value) {
        return Names.classToName(value.getClass());
    }

    SimpleProxyFactory getProxyFactory() {
        return getMapperContext().getProxyFactory();
    }
}
