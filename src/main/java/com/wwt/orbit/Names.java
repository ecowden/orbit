package com.wwt.orbit;

import java.lang.reflect.Method;

/**
 * Utilities for working with names of classes and methods
 */
abstract class Names {

    private static final String GET = "get";
    private static final String IS = "is";

    static String classToName(Class<?> clazz) {
        String className = clazz.getSimpleName();
        return Names.toCamelCase(className);
    }

    static String toCamelCase(String name) {
        String firstLetter = name.substring(0, 1).toLowerCase();
        return firstLetter + name.substring(1, name.length());
    }

    static String methodToName(Method method) {
        String methodName = method.getName();
        String name = removeAccessorPrefixes(methodName);
        return toCamelCase(name);
    }

    private static String removeAccessorPrefixes(String methodName) {
        if (methodName.startsWith(GET)) {
            String prefix = GET;
            return removePrefix(methodName, prefix);
        } else if (methodName.startsWith(IS)) {
            return removePrefix(methodName, IS);
        }
        return methodName;
    }

    private static String removePrefix(String s, String prefix) {
        return s.substring(prefix.length());
    }
}
