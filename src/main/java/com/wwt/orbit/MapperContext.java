package com.wwt.orbit;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Holder for dependencies which can be required by {@code Mapper} and its various subclasses.
 * <p>
 * This idea is a workaround for not being able to set dependencies via dependency injection.  The root problem is
 * that the fluent API we want requires people to be able to call {@code new MappingFunction() { ... }}.  Since
 * those MappingFunctions have dependencies, but the DI system can't create them, and therefore can't (easily)
 * inject them.
 * </p>
 * <p>
 * We should continue looking for alternate approaches that better decouple things.  One idea would be to use
 * method injection (an init method, for instance).  The drawback is that we would have to keep passing the
 * {@code Injector} instance along so that each {@code Mapper} could inject its children.  I'm not convinced
 * this is a significant improvement, so I'm leaving it alone for now.
 * </p>
 */
class MapperContext {
    private final Provider<Node> nodeProvider;
    private final SimpleProxyFactory proxyFactory;

    @Inject
    MapperContext(Provider<Node> nodeProvider, SimpleProxyFactory proxyFactory) {
        this.nodeProvider = nodeProvider;
        this.proxyFactory = proxyFactory;
    }

    Provider<Node> getNodeProvider() {
        return nodeProvider;
    }

    SimpleProxyFactory getProxyFactory() {
        return proxyFactory;
    }
}
