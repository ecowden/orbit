package com.wwt.orbit;

import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.inject.Inject;

/**
 * Maps an object or object graph to a heirarchical structure that can be easily serialized.
 * <p/>
 * <p>
 * If you have a User object with id and name properties:
 * <pre>
 *     Node root = Orbit.start()
 *       .set(user, new MappingFunction<User>() {
 *         public void map() {
 *           set(valueOf().getId());
 *           set(valueOf().getName());
 *         }
 *     }).getRootNode();
 *     Node userNode = (Node) root.get("user");
 *     assertThat((Integer) userNode.get("id"), is(42));
 *     assertThat((String) userNode.get("name"), is("Test User"));
 *    </pre>
 * </p>
 * <p>
 * In this example, Orbit uses the User class's name for the "user" field, and grabs the names for the "id" and "name"
 * fields from the invocation of {@code getId()} and {@code getName} methods.
 * </p>
 * <p>
 * There are many other ways to use Orbit, including explicitly specifying field names and values and mapping
 * collections of beans.  For more examples, see the {@code AcceptanceTest} class in the test suite.
 * </p>
 */
public class Orbit extends Mapper {

    private static final Injector INJECTOR = Guice.createInjector(new OrbitModule());

    private final Node rootNode;
    private final MapperContext mapperContext;

    public static Orbit start() {
        /*
         * We use our DI tool (Guice) to bootstrap the root object from a static factory method.  This may seem bad at
         * first, but it is the recommended best practice.  It isolates the outside world from any knowledge of our
         * objects, their dependencies, etc.  At the same time, we can still work with our classes
         * from within the project independently and safely.
         */
        return INJECTOR.getInstance(Orbit.class);
    }

    /**
     * Deliberately prevent public construction.  Use static factory method instead.
     */
    @Inject
    Orbit(MapperContext mapperContext) {
        this.mapperContext = mapperContext;
        this.rootNode = createNode();
    }

    /**
     * @return the root node of the Orbit.  This is generally used to get the result of the mapping.
     */
    public Node getRootNode() {
        return rootNode;
    }

    @Override
    MapperContext getMapperContext() {
        return mapperContext;
    }

    /**
     * Get a {@code Jsonifier} instance that can be used to serialize a {@code Node} or {@code Nodes} to JSON.
     * <p>
     * The current implementation uses Jackson 2.x.  This will allow users to automagically serialize
     * more than just primitives in their {@code Node}s.  This is, however, subject to change.
     * </p>
     */
    public static Jsonifier getJsonifier() {
        return INJECTOR.getInstance(JacksonJsonifier.class);
    }
}
