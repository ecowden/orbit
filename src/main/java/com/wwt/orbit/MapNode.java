package com.wwt.orbit;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.util.LinkedHashMap;

/**
 * Normally, I would <i>compose</i> this class with a LinkedHashMap instead of extending one.  In this case, however,
 * it is a convenient shortcut that allows us to do JSON serialization without additional configuration.
 * This will likely change in the future as I improve the JSON serialization approach.
 * <p/>
 * <p>In the meantime, developers should be cautious that changes to this class do not break serialization.</p>
 */
public class MapNode extends LinkedHashMap<String, Object> implements Node {

    /**
     * Deliberately restricted constructor; use dependency injection to create instances
     */
    MapNode() {

    }

    MapNode(String name, Object value) {
        put(name, value);
    }

    @Override
    public Object put(String name, Object value) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(name), "name cannot be null or empty");
        return super.put(name, value);
    }

}
