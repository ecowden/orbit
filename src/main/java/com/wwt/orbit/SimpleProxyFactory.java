package com.wwt.orbit;

import javassist.util.proxy.MethodFilter;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;
import org.objenesis.instantiator.ObjectInstantiator;

import javax.inject.Inject;
import java.lang.reflect.Method;

/**
 * Utility to streamline creation of class proxies using bytecode manipulation
 */
class SimpleProxyFactory {


    private final InstantiatorFactory instantiatorFactory;

    @Inject
    SimpleProxyFactory(InstantiatorFactory instantiatorFactory) {
        this.instantiatorFactory = instantiatorFactory;
    }


    @SuppressWarnings("unchecked")
    <T> T createProxy(Class<?> clazz, MethodHandler methodHandler) {
        Class c = createProxyClass(clazz);
        Object proxy = instantiateProxyClass(c);
        ((Proxy) proxy).setHandler(methodHandler);
        return (T) proxy;
    }

    private Object instantiateProxyClass(Class proxyClass) {
        /*
         * TODO according to the Objenesis documentation, ObjectInstantiator instances
         * should be cached and reused.
         */
        ObjectInstantiator instantiator = getInstantiator(proxyClass);
        return instantiator.newInstance();
    }

    private ObjectInstantiator getInstantiator(Class proxyClass) {
        return instantiatorFactory.getInstantiator(proxyClass);
    }

    private Class createProxyClass(Class<?> clazz) {
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setSuperclass(clazz);
        proxyFactory.setFilter(new IgnoreFinalizeMethodFilter());
        return proxyFactory.createClass();
    }


    private static class IgnoreFinalizeMethodFilter implements MethodFilter {
        @Override
        public boolean isHandled(Method m) {
            // ignore finalize()
            return !m.getName().equals("finalize");
        }
    }
}
