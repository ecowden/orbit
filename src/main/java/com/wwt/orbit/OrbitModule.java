package com.wwt.orbit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;

/**
 * Guice module for the project
 */
class OrbitModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Node.class).to(MapNode.class);
        bind(SimpleProxyFactory.class).in(Scopes.SINGLETON);
        bind(MapperContext.class).in(Scopes.SINGLETON);

        bind(Objenesis.class).to(ObjenesisStd.class).in(Scopes.SINGLETON);

        bind(ObjectMapper.class).in(Scopes.SINGLETON);
        bind(Jsonifier.class).to(JacksonJsonifier.class).in(Scopes.SINGLETON);
    }
}
