package com.wwt.orbit;

/**
 * The basic unit of Orbit's data structure, think of a heirarchy of {@code Node}s like a map (of maps (of maps...))
 */
public interface Node {
    /**
     * Create a field in the node with the specified name and value.  Name cannot be null or empty, although the value can.
     */
    Object put(String Name, Object value);

    /**
     * Get the value of the field with the specified name.  Note that you will likely have to cast the result.
     */
    Object get(Object Name);

}
