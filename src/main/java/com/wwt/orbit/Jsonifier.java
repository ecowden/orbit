package com.wwt.orbit;

/**
 * Converts {@code Node}s to JSON
 */
public interface Jsonifier {
    String toJson(Node root);

    String toJson(Iterable<Node> nodes);
}
