package com.wwt.orbit;

import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.objenesis.Objenesis;
import org.objenesis.instantiator.ObjectInstantiator;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.ExecutionException;

/**
 * Factory for Objenesis {@code ObjectInstantiator}s which caches instances by class
 * <p>
 * This follows the recommendations of the Objenesis project to reuse {@code ObjectInstantiator}s.  While the
 * {@code Objenesis} class itself can also be configured to cache instances, it does not appear
 * to be thread-safe.
 * </p>
 */
@Singleton
class InstantiatorFactory {

    private final Objenesis objenesis;
    private final LoadingCache<Class<?>, ObjectInstantiator> instantiatorCache = CacheBuilder.newBuilder()
            .build(new InstantiatorLoader());

    @Inject
    InstantiatorFactory(Objenesis objenesis) {
        this.objenesis = objenesis;
    }

    public ObjectInstantiator getInstantiator(Class<?> clazz) {
        try {
            return instantiatorCache.get(clazz);
        } catch (ExecutionException e) {
            // It cleans up the API significantly to avoid a typed exception here
            throw Throwables.propagate(e);
        }
    }

    private class InstantiatorLoader extends CacheLoader<Class<?>, ObjectInstantiator> {
        @Override
        public ObjectInstantiator load(Class<?> key) throws Exception {
            return objenesis.getInstantiatorOf(key);
        }
    }
}
