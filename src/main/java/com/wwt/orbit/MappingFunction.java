package com.wwt.orbit;

import javassist.util.proxy.MethodHandler;

import java.lang.reflect.Method;

public abstract class MappingFunction<T> extends Mapper {
    private T obj;
    private Node rootNode;
    private Method lastInvokedMethod;
    private MapperContext mapperContext;
    private T objProxy;

    public abstract void map();

    protected T valueOf() {
        return getProxy();
    }

    private T getProxy() {
        if (objProxy == null) {
            objProxy = createProxy();
        }
        return objProxy;
    }

    private T createProxy() {
        return getProxyFactory().createProxy(obj.getClass(), new TrackingForwardingMethodHandler());
    }

    @Override
    public Mapper set(Object value) {
        if (lastInvokedMethod != null) {
            String name = Names.methodToName(lastInvokedMethod);
            lastInvokedMethod = null;
            return super.set(name, value);
        }
        lastInvokedMethod = null;
        return super.set(value);
    }

    private void trackInvocation(Method m) {
        lastInvokedMethod = m;
    }

    public Node getRootNode() {
        return rootNode;
    }

    void evaluate(T obj, Node node) {
        this.obj = obj;
        this.rootNode = node;
        map();
    }

    void setMapperContext(MapperContext mapperContext) {
        this.mapperContext = mapperContext;
    }

    @Override
    MapperContext getMapperContext() {
        return mapperContext;
    }

    /**
     * Method Handler implementation that will record the last method invocation,
     * then forward the invocation onto our original object
     */
    private class TrackingForwardingMethodHandler implements MethodHandler {

        @Override
        public Object invoke(Object self, Method m, Method proceed, Object[] args) throws Throwable {
            trackInvocation(m);
            return m.invoke(obj, args);  // execute the original method.
        }
    }

}
