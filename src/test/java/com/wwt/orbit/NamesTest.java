package com.wwt.orbit;

import com.wwt.orbit.stub.User;
import org.junit.Test;

import java.util.LinkedHashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class NamesTest {

    @Test
    public void testClassToName() {
        String actual = Names.classToName(LinkedHashMap.class);
        assertThat(actual, is("linkedHashMap"));
    }

    @Test
    public void testMethodToName_startsWithGet() throws Exception {
        String actual = Names.methodToName(User.class.getMethod("getId"));
        assertThat(actual, is("id"));
    }

    @Test
    public void testMethodToName_startsWithIs() throws Exception {
        String actual = Names.methodToName(String.class.getMethod("isEmpty"));
        assertThat(actual, is("empty"));
    }

    @Test
    public void testMethodToName_noMeaningfulPrefix() throws Exception {
        String actual = Names.methodToName(String.class.getMethod("length"));
        assertThat(actual, is("length"));
    }

    @Test
    public void testToCamelCase() throws Exception {
        String actual = Names.toCamelCase("HelloWorld");
        assertThat(actual, is("helloWorld"));
    }


}
