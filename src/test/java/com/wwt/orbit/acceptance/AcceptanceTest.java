package com.wwt.orbit.acceptance;

import com.google.common.collect.Lists;
import com.wwt.orbit.MappingFunction;
import com.wwt.orbit.Node;
import com.wwt.orbit.Orbit;
import com.wwt.orbit.stub.User;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Acceptance tests which demonstrate the API we want and verify that all structures are serializable
 * <p>
 * It's important to keep this out of the main package so that we don't accidentally use package private
 * members.
 * </p>
 */
public class AcceptanceTest {

    @Test
    public void testSimpleApi() throws Exception {
        User user = new User(42, "Test User");

        Node root = Orbit.start()
                .set("user", user, new MappingFunction<User>() {
                    public void map() {
                        set("id", valueOf().getId());
                        set("name", valueOf().getName());
                    }
                }).getRootNode();

        Node userNode = (Node) root.get("user");

        assertThat((Integer) userNode.get("id"), is(42));
        assertThat((String) userNode.get("name"), is("Test User"));

        String json = Orbit.getJsonifier().toJson(root);
        assertThat(json, is("{\"user\":{\"id\":42,\"name\":\"Test User\"}}"));
    }


    @Test
    public void testFullyReflectiveApi() throws Exception {
        User user = new User(42, "Test User");

        Node root = Orbit.start()
                .set(user, new MappingFunction<User>() {
                    public void map() {
                        set(valueOf().getId());
                        set(valueOf().getName());
                    }
                }).getRootNode();

        Node userNode = (Node) root.get("user");

        assertThat((Integer) userNode.get("id"), is(42));
        assertThat((String) userNode.get("name"), is("Test User"));

        String json = Orbit.getJsonifier().toJson(root);
        assertThat(json, is("{\"user\":{\"id\":42,\"name\":\"Test User\"}}"));
    }

    @Test
    public void testListOf() throws Exception {
        Iterable<User> users = Lists.newArrayList(
                new User(1, "Test User 1"),
                new User(2, "Test User 2")
        );

        Node rootNode = Orbit.start()
                .list("users", users, new MappingFunction<User>() {
                    @Override
                    public void map() {
                        set(valueOf().getId());
                        set(valueOf().getName());
                    }
                })
                .getRootNode();

        @SuppressWarnings("unchecked")
        List<Node> userNodes = (List<Node>) rootNode.get("users");
        assertThat(userNodes.size(), is(2));

        Node userNode1 = userNodes.get(0);
        assertThat((Integer) userNode1.get("id"), is(1));
        assertThat((String) userNode1.get("name"), is("Test User 1"));

        Node userNode2 = userNodes.get(1);
        assertThat((Integer) userNode2.get("id"), is(2));
        assertThat((String) userNode2.get("name"), is("Test User 2"));

        String json = Orbit.getJsonifier().toJson(rootNode);
        assertThat(json, is("{\"users\":[{\"id\":1,\"name\":\"Test User 1\"},{\"id\":2,\"name\":\"Test User 2\"}]}"));
    }

    @Test
    public void testMultiLevelIntegration() throws Exception {
        final User user1 = new User(1, "User One");
        final User user2 = new User(2, "User Two");

        Node root = Orbit.start()
                .set(user1, new MappingFunction<User>() {
                    public void map() {
                        set("id", valueOf().getId());
                        set("name", valueOf().getName());
                        set("child", user2, new MappingFunction<User>() {
                            @Override
                            public void map() {
                                set("id", valueOf().getId());
                                set("name", valueOf().getName());
                            }
                        });
                    }
                }).getRootNode();

        Node user1Node = (Node) root.get("user");

        assertThat((Integer) user1Node.get("id"), is(1));
        assertThat((String) user1Node.get("name"), is("User One"));

        Node user2Node = (Node) user1Node.get("child");
        assertThat((Integer) user2Node.get("id"), is(2));
        assertThat((String) user2Node.get("name"), is("User Two"));

        String json = Orbit.getJsonifier().toJson(root);
        assertThat(json, is("{\"user\":{\"id\":1,\"name\":\"User One\",\"child\":{\"id\":2,\"name\":\"User Two\"}}}"));
    }
}
