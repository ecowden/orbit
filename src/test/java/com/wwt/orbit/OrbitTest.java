package com.wwt.orbit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class OrbitTest {

    @Test
    public void testSetDirectNameAndValue() throws Exception {
        Node root = Orbit.start()
                .set("name", "value")
                .getRootNode();

        assertThat((String) root.get("name"), is("value"));
    }

    @Test
    public void testSetWithoutNameUsesClassName() throws Exception {
        Node root = Orbit.start()
                .set("value")
                .getRootNode();

        assertThat((String) root.get("string"), is("value"));
    }

    @Test(expected = NullPointerException.class)
    public void testSetWithoutNameUsesClassName_RejectsNullValues() throws Exception {
        Orbit.start()
                .set(null);
    }

    @Test(expected = Exception.class)
    public void testSetRejectsNullNames() throws Exception {
        /*
         * Redundant now that the Node contract catches this condition, but I still like it for safety.
         */
        Orbit.start()
                .set(null, "kaBoom");
    }

    @Test
    public void testGetJsonifier() throws Exception {
        Jsonifier actual = Orbit.getJsonifier();

        assertThat(actual, is(notNullValue()));
    }
}
