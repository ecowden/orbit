package com.wwt.orbit;

import org.junit.Test;

import javax.inject.Provider;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class MapperContextTest {

    @Test
    public void testCreationAndAccess() throws Exception {
        @SuppressWarnings("unchecked")
        Provider<Node> nodeProvider = mock(Provider.class);
        SimpleProxyFactory proxyFactory = mock(SimpleProxyFactory.class);

        MapperContext instance = new MapperContext(nodeProvider, proxyFactory);

        assertThat(instance.getNodeProvider(), is(nodeProvider));
        assertThat(instance.getProxyFactory(), is(proxyFactory));
    }
}
