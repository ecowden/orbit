package com.wwt.orbit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MapNodeTest {
    @Test
    public void testPutAndGet() throws Exception {
        MapNode instance = new MapNode();
        instance.put("name", "value");

        assertThat((String) instance.get("name"), is("value"));
    }

    @Test
    public void testPutConstructor() throws Exception {
        MapNode instance = new MapNode("name", "value");
        assertThat((String) instance.get("name"), is("value"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutRejectsNullNames() throws Exception {
        new MapNode().put(null, "kaBoom");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPutRejectsEmptyNames() throws Exception {
        new MapNode().put("", "kaBoom");
    }

}
