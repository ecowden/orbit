package com.wwt.orbit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class JacksonJsonifierTest {

    private JacksonJsonifier instance;

    @Before
    public void setUp() throws Exception {
        instance = new JacksonJsonifier(new ObjectMapper());
    }

    @Test
    public void testToJson_SimpleNode() throws Exception {
        Node userNode = createUserNode(42, "Test User");

        Node root = new MapNode("user", userNode);

        String actual = instance.toJson(root);

        // This assertion is sensitive to formatting
        assertThat(actual, is("{\"user\":{\"id\":42,\"name\":\"Test User\"}}"));
    }

    @Test
    public void testToJson_ListOfNodes() throws Exception {
        ArrayList<Node> nodes = Lists.newArrayList(
                createUserNode(1, "Test User 1"),
                createUserNode(2, "Test User 2")
        );

        String actual = instance.toJson(nodes);

        // This assertion is sensitive to formatting
        assertThat(actual, is("[{\"id\":1,\"name\":\"Test User 1\"},{\"id\":2,\"name\":\"Test User 2\"}]"));
    }

    private Node createUserNode(int id, String name) {
        MapNode result = new MapNode();
        result.put("id", id);
        result.put("name", name);
        return result;
    }
}
