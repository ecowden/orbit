package com.wwt.orbit;

import com.wwt.orbit.stub.User;
import javassist.util.proxy.MethodHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class MappingFunctionTest {

    @Mock
    SimpleProxyFactory proxyFactory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testValueOfCreatesProxy() throws Exception {
        final User expectedUser = mock(User.class);
        whenProxyFactoryCreatesUserProxyItShouldReturn(expectedUser);

        MappingFunction<User> instance = new MappingFunction<User>() {
            @Override
            public void map() {
                User actualUser = valueOf();
                assertThat(actualUser, is(expectedUser));
            }
        };

        setUpAndEvaluateMappingFunction(instance);
    }

    @Test
    public void testValueOfReusesProxyInstances() throws Exception {
        final User expectedUser1 = mock(User.class);
        final User expectedUser2 = mock(User.class);
        whenProxyFactoryCreatesUserProxyItShouldReturn(expectedUser1, expectedUser2);

        MappingFunction<User> instance = new MappingFunction<User>() {
            @Override
            public void map() {
                User actualUser1 = valueOf();
                User actualUser2 = valueOf();
                assertThat(actualUser1, is(expectedUser1));
                assertThat(actualUser2, is(expectedUser1));
            }
        };

        setUpAndEvaluateMappingFunction(instance);
        verify(proxyFactory, times(1)).createProxy(Matchers.any(Class.class), Matchers.any(MethodHandler.class));
    }

    private void setUpAndEvaluateMappingFunction(MappingFunction<User> instance) {
        instance.setMapperContext(new MapperContext(null, proxyFactory));
        instance.evaluate(new User(42, "Test User"), new MapNode());
    }

    private void whenProxyFactoryCreatesUserProxyItShouldReturn(User firstExpectedUser, User... otherExpectedUsers) {
        when(proxyFactory.createProxy(Matchers.eq(User.class), Matchers.any(MethodHandler.class)))
                .thenReturn(firstExpectedUser, new Object[]{otherExpectedUsers});
    }
}
