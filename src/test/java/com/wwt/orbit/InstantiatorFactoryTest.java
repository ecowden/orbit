package com.wwt.orbit;

import com.wwt.orbit.stub.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.objenesis.Objenesis;
import org.objenesis.instantiator.ObjectInstantiator;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InstantiatorFactoryTest {

    @InjectMocks
    InstantiatorFactory instance;

    @Mock
    Objenesis objenesis;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetInstantiatorReturnsInstanceOfClass() throws Exception {
        ObjectInstantiator instantiator = mock(ObjectInstantiator.class);
        when(objenesis.getInstantiatorOf(User.class)).thenReturn(instantiator);

        ObjectInstantiator actual = instance.getInstantiator(User.class);

        assertThat(actual, is(instantiator));
    }

    @Test
    public void testGetInstantiator_cachesInstantiatorsForAType() throws Exception {
        ObjectInstantiator instantiator1 = mock(ObjectInstantiator.class);
        ObjectInstantiator instantiator2 = mock(ObjectInstantiator.class);
        when(objenesis.getInstantiatorOf(User.class))
                .thenReturn(instantiator1)
                .thenReturn(instantiator2);

        ObjectInstantiator actual1 = instance.getInstantiator(User.class);
        ObjectInstantiator actual2 = instance.getInstantiator(User.class);

        assertThat(actual2, is(actual1));
    }

    @Test
    public void testGetInstantiator_returnsDifferentInstantiatorsForDifferentClasses() throws Exception {
        ObjectInstantiator instantiator1 = mock(ObjectInstantiator.class);
        ObjectInstantiator instantiator2 = mock(ObjectInstantiator.class);
        when(objenesis.getInstantiatorOf(User.class)).thenReturn(instantiator1);
        when(objenesis.getInstantiatorOf(ArrayList.class)).thenReturn(instantiator2);

        ObjectInstantiator actual1 = instance.getInstantiator(User.class);
        ObjectInstantiator actual2 = instance.getInstantiator(ArrayList.class);

        assertThat(actual2, not(is(actual1)));
    }
}
