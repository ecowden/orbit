package com.wwt.orbit;

import com.wwt.orbit.stub.User;
import javassist.util.proxy.MethodHandler;
import org.junit.Before;
import org.junit.Test;
import org.objenesis.ObjenesisStd;

import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SimpleProxyFactoryTest {

    SimpleProxyFactory instance;

    @Before
    public void setUp() throws Exception {
        /*
         * Use a real ObjectInstantiator to verify correct interaction with third-party library.  Also: mocking
         * the behavior with the proxy subclasses is pretty messy.
         */
        InstantiatorFactory instantiatorFactory = new InstantiatorFactory(new ObjenesisStd());
        instance = new SimpleProxyFactory(instantiatorFactory);

    }

    @Test
    public void testProxyInvokesMethodHandler() throws Exception {
        User proxy = instance.createProxy(User.class, new MethodHandler() {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
                return thisMethod.getName();
            }
        });

        String actual = proxy.getName();
        assertThat(actual, is("getName"));
    }

    @Test
    public void testCanProxyClassWithoutDefaultConstructor() throws Exception {
        HasNoDefaultConstructor proxy = instance.createProxy(HasNoDefaultConstructor.class, new MethodHandler() {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
                return thisMethod.getName();
            }
        });

        String actual = proxy.toString();
        assertThat(actual, is("toString"));
    }

    @SuppressWarnings("unused")
    private static class HasNoDefaultConstructor {
        private HasNoDefaultConstructor(String anArgumentSoThatThisIsNotADefaultConstructor) {
        }
    }

}
